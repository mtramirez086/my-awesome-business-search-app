# My awesome business search app

A simple business search app written in Swift 5 with Xcode Version 11.3.1

**Getting started**

The application is using dependency managet for Cocoapods, which are pushed to repo hence pod install is optional.

**Prequisites**

Open workspace in Xcode version 11.3.1

**Copyrights**

The application contents are free to use.

